/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jinventory;

import avik.jShop.bo.ui.frames.StockEntryPane;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author avik
 */
public class JInventory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                JFrame frame=new JFrame();
                frame.add(new StockEntryPane());
                frame.setBounds(0, 0, 800, 800);
                
                frame.setVisible(true);
            }
        });
        
    }
    
}
