/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avik.jShop.bo.ui.frames;

/**
 *
 * @author avik
 */


import java.awt.Color;
import javax.swing.*;
import java.awt.Dimension;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


public class TablePanel extends JPanel { 
                                 
    private JTable table;
    DefaultTableModel dtm;
    int selectedRowIndex;
    String productID;
    String productModelNumber;

    public String getProductID() {
        return productID;
    }

    public String getProductModelNumber() {
        return productModelNumber;
    }
    
    
    private String[] columnNames = {
                                        "Product Model No.",
                                        "Product ID",
                                        "Type",
                                        "Manufacturer",
                                        "MRP",
                                        "VAT %",
                                        "Price"
                                       
                                    };
    
    

    public TablePanel() {
        super();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        dtm=new DefaultTableModel(columnNames,0);
        table = new JTable(dtm){
            @Override
            public boolean isCellEditable(int rowIndex,int colIndex)
            {
                return false;
            }
            
        };
        table.setPreferredScrollableViewportSize(new Dimension(500, 100));
        table.setFillsViewportHeight(true);
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setGridColor(Color.blue);
        table.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent lse) {
                
                if(lse.getValueIsAdjusting())
                {
                    int selectedRowIndex=table.getSelectedRow();
                    System.out.println(selectedRowIndex);
                    productID=(String) table.getValueAt(selectedRowIndex,1);
                    productModelNumber=(String) table.getValueAt(selectedRowIndex, 0);
                    
                }
            }
            
        }
                
                
                
                );
        add(new JScrollPane(table));
        
    

       
    }

    public void addRow(Object[] rowData)
    {
        dtm.addRow(rowData);
    }
    
    public int getSelectedRow()
    {
        return table.getSelectedRow();
    }
    public void deleteRow(int rowId)
    {
        
            
            dtm.removeRow(rowId);
        
    }
    public void deleteAllRow()
    {
        dtm.setRowCount(0);
    }

    
    
    
    
}
