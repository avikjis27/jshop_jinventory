/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * StockEntryPane.java
 *
 * Created on 13 Jan, 2013, 7:02:06 PM
 */
package avik.jShop.bo.ui.frames;

import avik.jShop.controller.StockEntryController;
import avik.jShop.dao.ItemFinder;
import java.awt.event.KeyEvent;
import persistency.ItemMaster;
import persistency.ItemType;
import persistency.Manufacturer;
import java.awt.Color;
import java.awt.event.KeyListener;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import persistency.StockReplenishmentDetails;

/**
 *
 * @author avik
 */
public class StockEntryPane extends javax.swing.JPanel implements KeyListener {

    /** Creates new form StockEntryPane */
    private StockEntryController sec;
    
    
    public StockEntryPane() {
        
        initComponents();
        if(initializeStockEntryPane())
        {
            System.out.println("Stock entry pane successfully initialized");
        }
        addListener();
        
    }
    
    private boolean initializeStockEntryPane()
    {
        transaction.setVisible(false);
        date.setText(new Date().toString());
        sec = StockEntryController.beginTransaction();
        refresh();
        enableButtons();
        return true;
    }
    private void addListener()
    {
        modelNumber.addKeyListener(this);
        itemSerialNumber.addKeyListener(this);
    }
    
    private void disableButtons()
    {   addAll.setEnabled(false);
        addCurrent.setEnabled(false);
        addItem.setEnabled(false);
        editManu.setEnabled(false);
        resetCurrent.setEnabled(false);
        removeItem.setEnabled(false);
        
    }
    private void enableButtons()
    {   addAll.setEnabled(true);
        addCurrent.setEnabled(true);
        addItem.setEnabled(true);
        editManu.setEnabled(true);
        resetCurrent.setEnabled(true);
        removeItem.setEnabled(true);
        
    }
    
    public boolean stockSanityController()
    {
        int result=sec.stockEntrySanityChecker(purchasePrice.getText(), MRP.getText(), vat.getText(), modelNumber.getText(), itemSerialNumber.getText());
        /*
         * 100:Purchase price is empty
         * 101:purchase price entry is not correct
         * 200:MRP is empty
         * 201:MRP entry is not correct
         * 300:model number is empty
         * 400:serial number is empty
         * 500:vat is empty
         * 501:vat entry is not correct
         * 0:  OK  
         */
        modelNumber.setBackground(Color.white);
        itemSerialNumber.setBackground(Color.white);
        MRP.setBackground(Color.white);
        vat.setBackground(Color.white);
        purchasePrice.setBackground(Color.white);
        
        if(result==0)
            return true;
        else
        {   
            
            if(result==300)
            {
                JOptionPane.showMessageDialog(this, "Model number should not be empty");
                modelNumber.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            if(result==400)
            {
                JOptionPane.showMessageDialog(this, "Serial number should not be empty");
                itemSerialNumber.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            if(result==100)
            {
                JOptionPane.showMessageDialog(this, "Purchase price is empty");
                purchasePrice.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            if(result==101)
            {
                JOptionPane.showMessageDialog(this, "Purchase price Entry not correct");
                purchasePrice.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            if(result==200)
            {
                JOptionPane.showMessageDialog(this, "MRP field empty");
                MRP.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            if(result==201)
            {
                JOptionPane.showMessageDialog(this, "MRP entry is not correct");
                MRP.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            if(result==500)
            {
                JOptionPane.showMessageDialog(this, "VAT Entry is empty");
                vat.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            if(result==501||result==502)
            {
                JOptionPane.showMessageDialog(this, "VAT entry is not correct");
                vat.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            if(result==600)
            {
                JOptionPane.showMessageDialog(this, "Purchase price is greater than MRP");
                purchasePrice.setBackground(Color.LIGHT_GRAY);
                MRP.setBackground(Color.LIGHT_GRAY);
                return false;
            }
            
            return false;
        }
    }
    
    public void refresh()
    {
        manufacture.removeAllItems();
        List<Manufacturer> allManufacturer=sec.getAllManufacturer();
        for(int i=0;i<allManufacturer.size();i++)
        {
            manufacture.addItem(allManufacturer.get(i).getManufacturerName());
            //System.out.println(allManufacturer.get(i).toString());
        }
        
        itemType.removeAllItems();
        List<ItemType> allItemType=sec.getAllItemType();
        for(int i=0;i<allItemType.size();i++)
        {
            itemType.addItem(allItemType.get(i).getItemName());
            //System.out.println(allManufacturer.get(i).toString());
        }
    }
    public void reset()
    {
        modelNumber.setText("");
        manufacture.setSelectedIndex(0);
        itemType.setSelectedIndex(0);
        purchasePrice.setText("");
        MRP.setText("");
        itemSerialNumber.setText("");
        vat.setText("");
        
        
    }
    
    @Override
    public void keyTyped(KeyEvent ke) {
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode()==112 && modelNumber.hasFocus() )
        {
            System.out.println(modelNumber.getText());
            List list=ItemFinder.getItems(modelNumber.getText());
            String selected = (String) JOptionPane.showInputDialog(this,"Items Found","List",JOptionPane.PLAIN_MESSAGE,null,list.toArray(),"List");
            modelNumber.setText(selected);
            
        }
      
        
            if(ke.getKeyCode()==112 && itemSerialNumber .hasFocus() )
            {
            System.out.println(modelNumber.getText());
            List list=ItemFinder.getPossibleSerial(modelNumber.getText(),itemSerialNumber.getText());
            String selected = (String) JOptionPane.showInputDialog(this,"Items Found","List",JOptionPane.PLAIN_MESSAGE,null,list.toArray(),"List");
            itemSerialNumber.setText(selected);
            }
        }

    @Override
    public void keyReleased(KeyEvent ke) {
       
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        sen = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        modelNumber = new javax.swing.JTextField();
        itemSerialNumber = new javax.swing.JTextField();
        vat = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        purchasePrice = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        manufacture = new javax.swing.JComboBox();
        itemType = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        MRP = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        removeItem = new javax.swing.JButton();
        addCurrent = new javax.swing.JButton();
        resetCurrent = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        editManu = new javax.swing.JButton();
        addItem = new javax.swing.JButton();
        addAll = new javax.swing.JButton();
        transaction = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        tablePanel = new avik.jShop.bo.ui.frames.TablePanel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(1231, 627));
        setPreferredSize(new java.awt.Dimension(1231, 627));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 153, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Stock Entry");

        date.setForeground(java.awt.SystemColor.activeCaption);
        date.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, null, new java.awt.Color(83, 227, 254), null, null));

        sen.setBorder(javax.swing.BorderFactory.createTitledBorder("Stock Entry Number"));

        jPanel1.setBackground(new java.awt.Color(255, 255, 216));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Details", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Agency FB", 1, 14), new java.awt.Color(51, 51, 255))); // NOI18N

        modelNumber.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        modelNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modelNumberActionPerformed(evt);
            }
        });

        itemSerialNumber.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        vat.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        jLabel3.setText("Manufacturer");

        jLabel8.setText("VAT");

        purchasePrice.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        jLabel5.setText("Purchase Price");

        jLabel6.setText("MRP");

        jLabel7.setText("Item Serial Number");

        jLabel2.setText("Item's Model Number");

        manufacture.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        manufacture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manufactureActionPerformed(evt);
            }
        });

        itemType.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        jLabel4.setText("Item Type");

        MRP.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5))
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(modelNumber, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(itemSerialNumber, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(manufacture, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(purchasePrice, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(itemType, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(MRP, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(vat, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(176, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel2)
                        .addGap(26, 26, 26)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(itemSerialNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(56, 56, 56)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(itemType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(purchasePrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(MRP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8)
                            .addComponent(vat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(modelNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(manufacture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(255, 226, 226));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Master Button", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Agency FB", 1, 14), new java.awt.Color(102, 102, 255))); // NOI18N

        removeItem.setText("Remove Item");
        removeItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeItemActionPerformed(evt);
            }
        });

        addCurrent.setText("Add Current");
        addCurrent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCurrentActionPerformed(evt);
            }
        });

        resetCurrent.setText("Reset Current");
        resetCurrent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetCurrentActionPerformed(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(255, 211, 211));

        editManu.setBackground(java.awt.Color.white);
        editManu.setForeground(java.awt.Color.red);
        editManu.setText("Add Manufacture");
        editManu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editManuActionPerformed(evt);
            }
        });

        addItem.setBackground(java.awt.Color.white);
        addItem.setForeground(java.awt.Color.red);
        addItem.setText("Add Item Type");
        addItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addItemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(editManu, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                    .addComponent(addItem, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editManu)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(addItem)
                .addContainerGap())
        );

        addAll.setText("Save All");
        addAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAllActionPerformed(evt);
            }
        });

        transaction.setText("Transaction");
        transaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transactionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(addCurrent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(resetCurrent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(removeItem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(addAll, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                    .addComponent(transaction, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(resetCurrent)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(addCurrent)
                .addGap(12, 12, 12)
                .addComponent(removeItem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(addAll)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(transaction)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
        );

        jPanel3.setBackground(new java.awt.Color(227, 213, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Details", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Agency FB", 1, 14), new java.awt.Color(51, 51, 255))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(date, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(93, 93, 93)
                        .addComponent(sen, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(244, 244, 244)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(sen)
                    .addComponent(date, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(22, 22, 22)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

private void addCurrentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCurrentActionPerformed
    
    String[] str={  
                    modelNumber.getText(),
                    itemSerialNumber.getText(),
                    itemType.getSelectedItem().toString(),
                    manufacture.getSelectedItem().toString(),
                    MRP.getText(),
                    vat.getText(),
                    purchasePrice.getText()
                    
            
    
                };
    if(stockSanityController())
    {
        ItemMaster itemMaster=new ItemMaster(modelNumber.getText(), itemSerialNumber.getText(),itemType.getSelectedItem().toString(),Float.valueOf(MRP.getText()),Float.valueOf(purchasePrice.getText()) ,Float.valueOf(vat.getText()),manufacture.getSelectedItem().toString() ); 
        sec.getSrd().getItems().add(itemMaster);
        tablePanel.addRow(str);
        purchasePrice.setText("");
        itemSerialNumber.setText("");
        //System.out.println();
    }
    
    
    
}//GEN-LAST:event_addCurrentActionPerformed

private void modelNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modelNumberActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_modelNumberActionPerformed

private void resetCurrentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetCurrentActionPerformed
// TODO add your handling code here:
    reset();
    
}//GEN-LAST:event_resetCurrentActionPerformed

private void addItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addItemActionPerformed
// TODO add your handling code here:
    String newItemType=JOptionPane.showInputDialog("Enter New Item Type");
    if(newItemType!=null &&  newItemType.equals(""))
    {
        JOptionPane.showMessageDialog(this,"Item Type should not be empty");
    }
    else if(newItemType!=null){
        ItemType itemType=new ItemType(newItemType.toUpperCase());
        if(sec.addNewItemType(itemType))
        {   System.out.println("Item successfully added");
            refresh();
            
        }
        else
            System.out.println("Item not added");
    }
    else
    {
        
    }
    
            
}//GEN-LAST:event_addItemActionPerformed

private void editManuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editManuActionPerformed
// TODO add your handling code here:
    String newManuType=JOptionPane.showInputDialog("Enter New Manufacturer Type");
    if(newManuType!=null && newManuType.equals(""))
    {
        JOptionPane.showMessageDialog(this,"Manufacture should not be empty");
    }
    else if(newManuType!=null){
        Manufacturer man=new Manufacturer(newManuType.toUpperCase());
        System.out.println("Going to add manufacturer:"+newManuType);
        if(sec.addManufacturer(man))
        {    System.out.println("Manufacturer successfully added");
             refresh();   

        }
        else
            System.out.println("Manufacturer not added");
    }
    else
    {
        
    }
    
}//GEN-LAST:event_editManuActionPerformed

private void removeItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeItemActionPerformed
// TODO add your handling code here:
    System.out.println("Going to remove rows");
    int row=tablePanel.getSelectedRow();
    //System.out.println(row);
    if(row < 0)
    {
        JOptionPane.showConfirmDialog(this,"No item to delete!!");
        
    }
    else{
        StockReplenishmentDetails srd =  sec.getSrd();
        srd.deleteItem(tablePanel.getProductID(), tablePanel.getProductModelNumber());
        tablePanel.deleteRow(row);
        //srd.deleteItem(tablePanel.get);
    }
    
   
    
}//GEN-LAST:event_removeItemActionPerformed

private void addAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAllActionPerformed
// TODO add your handling code here:
    int confirm=JOptionPane.showConfirmDialog(this,"Save current Transaction");
    if(confirm==0)
    {   disableButtons();
        if(sec.saveStock())
        {
            sec=null;
            StockEntryController.closeTransaction();
            tablePanel.deleteAllRow();
            JOptionPane.showMessageDialog(this,"Transaction Saved");
            int confirmNewTran=JOptionPane.showConfirmDialog(this,"Create New Transaction");
            if(confirmNewTran==0)
            {
                initializeStockEntryPane();
                enableButtons();
                refresh();
            }
            else
            {
                transaction.setVisible(true);
            }
        }
        else
        {
            sec=null;
            StockEntryController.closeTransaction();
            tablePanel.deleteAllRow();
            JOptionPane.showMessageDialog(this,"Transaction Crashed Try Again!");
            int confirmNewTran=JOptionPane.showConfirmDialog(this,"Create New Transaction");
            if(confirmNewTran==0)
            {
                initializeStockEntryPane();
                enableButtons();
                refresh();
            }
            else
            {
                 transaction.setVisible(true);
            }
        }
        
    }
    
}//GEN-LAST:event_addAllActionPerformed

    private void manufactureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manufactureActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_manufactureActionPerformed

    private void transactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transactionActionPerformed
        // TODO add your handling code here:
        initializeStockEntryPane();
        enableButtons();
        refresh();
    }//GEN-LAST:event_transactionActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField MRP;
    private javax.swing.JButton addAll;
    private javax.swing.JButton addCurrent;
    private javax.swing.JButton addItem;
    private javax.swing.JLabel date;
    private javax.swing.JButton editManu;
    private javax.swing.JTextField itemSerialNumber;
    private javax.swing.JComboBox itemType;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JComboBox manufacture;
    private javax.swing.JTextField modelNumber;
    private javax.swing.JTextField purchasePrice;
    private javax.swing.JButton removeItem;
    private javax.swing.JButton resetCurrent;
    private javax.swing.JLabel sen;
    private avik.jShop.bo.ui.frames.TablePanel tablePanel;
    private javax.swing.JButton transaction;
    private javax.swing.JTextField vat;
    // End of variables declaration//GEN-END:variables

    
}
