/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avik.jShop.controller;

import persistency.ItemType;
import persistency.Manufacturer;
import persistency.HibernateActivity;
import persistency.StockReplenishmentDetails;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;


/**
 *
 * @author avik
 */
public class StockEntryController {
    
    private static StockReplenishmentDetails srd;
    private String replenishmentNumber;
    private static boolean isOpenTransaction=false;
    private static StockEntryController sec;
    
    private StockEntryController()
    {
        
        
        if(srd==null)
        {
            setSrd(srd);
        }
    }
    public static StockEntryController beginTransaction()
    {
        if(StockEntryController.isOpenTransaction && sec!=null )
        {
            System.out.println("Continuing with previous EntryControllingTransaction");
            return sec;
        }
        else{
             sec=new StockEntryController();
             isOpenTransaction=true;
             if(sec!=null&& isOpenTransaction)
             {  
                System.out.println("New EntryControllingTransaction has been created:"+sec);
                return sec;
             }
             else{
                 System.out.println("New EntryControllingTransaction can't be created:");
                 return null;
             }
        }
        
        
    }
    public static boolean closeTransaction()
    {
        if(!isOpenTransaction||sec==null)
        {
            System.out.println("No open transaction or has already been closed");
            return false;
        }
        else
        {
            System.out.println("Going to close transaction:"+sec);
            isOpenTransaction=false;
            sec=null;
            System.out.println("Going to close Replenishment deatils:"+srd);
            if(srd!=null)
            {   StockReplenishmentDetails.closeStockReplenishmentDetailsFactory(); 
                srd=null;
            
            }
            else
            {    System.out.println("Replenishment deatils already closed");
                return false;
            }
            System.out.println("Transaction has been successfully closed");
            return true;
        }
    }

    public StockReplenishmentDetails getSrd() {
        return srd;
    }

    private void setSrd(StockReplenishmentDetails srd) {
        this.srd =  StockReplenishmentDetails.stockReplenishmentDetailsFactory();
    }
    
    
    public List getAllManufacturer() {
        
        Session session=HibernateActivity.getHibernateSession();
        
        if(session!=null)
        {   
            session.beginTransaction();
            List<Manufacturer> manufacturers=(List<Manufacturer>) session.createQuery("from Manufacturer").list();
            
            session.getTransaction().commit();
            
            
            
            
            return manufacturers;
        }
        else
            return null;
        
    }
    

    public static List<ItemType> getAllItemType() {
        Session session=HibernateActivity.getHibernateSession();
        
        if(session!=null)
        {   
            session.beginTransaction();
            List<ItemType> itemType=(List<ItemType>) session.createQuery("from ItemType").list();
            
            session.getTransaction().commit();
            
            
            
            
            return itemType;
        }
        else
            return null;
    }
    public String getReplinishmentNumber()
    {
        long sid=srd.getStockReplenishmentNumber();
        Date dt=new Date();
        return sid+"#"+dt.getDate()+dt.getMonth()+dt.getYear();
        //return ""+dt.getDate();
    }
    public int stockEntrySanityChecker(String purchasePrice,String MRP,String vat,String modelNumber,String serialNumber)
    {   int result=0;
        /*
         * 100:Purchase price is empty
         * 101:purchase price entry is not correct
         * 200:MRP is empty
         * 201:MRP entry is not correct
         * 300:model number is empty
         * 400:serial number is empty
         * 500:vat is empty
         * 501:vat entry is not correct
         * 502:vat should not be greater that 100
         * 600:Purchase price is less than MRP
         * 0:  OK  
         */
        
        if(purchasePrice.equals(""))
        {    result=100;
             return result;
        }
        else{
            try{
                Float.valueOf(purchasePrice);
              
            }
            catch(NumberFormatException nfe)
            {
                result=101;
                return result;
            }
        }
        
        if(MRP.equals(""))
        {    result=200;
             return result;
        }
        else{
            try{
                Float.valueOf(MRP);
              
            }
            catch(NumberFormatException nfe)
            {
                result=201;
                return result;
            }
        }
        
        if(vat.equals(""))
        {    result=500;
             return result;
        }
        else{
            try{
                if(Float.valueOf(vat)>100)
                {    result=502;
                     return result;
                }   
              
            }
            catch(NumberFormatException nfe)
            {
                result=501;
                return result;
            }
        }
        if(modelNumber.equals(""))
        {
            result=300;
            return result;
        }
        if(serialNumber.equals(""))
        {
            result=400;
            return result;
        }
        if(Float.valueOf(purchasePrice)>Float.valueOf(MRP))
        {
           result=600;
           return result;
        }
        
        return result;
    }
   
    public boolean saveStock()
    {
        Session session=HibernateActivity.getHibernateSession();
        
        if(session!=null)
        {   
            try{
            if(srd.getItems().size()>0)
            {
                System.out.println("Number of items going to save:"+srd.getItems().size());
                session.beginTransaction();
                session.save(srd);
                session.getTransaction().commit();
                
                return true;
            }
            else
            {
                System.out.println("No item to save!!");
                return false;
            }
                
            
            }
            catch(HibernateException he)
            {
                session.getTransaction().rollback();
                HibernateActivity.closeHibernateSession();
                System.out.println("Transaction has been rolled back");
                return false;
            }
            
            
            
            
            
        }
        else
        {    
            System.out.println("Cannot find hibernate session while saving stock");
            return false;
        }
    }
    
    public boolean addManufacturer(Manufacturer manufacturer)
    {
        Session session=HibernateActivity.getHibernateSession();
        
        if(session!=null)
        {   
            session.beginTransaction();
            session.save(manufacturer);
            session.getTransaction().commit();
            
            
            return true;
        }
        else
            return false;
    }

    public boolean addNewItemType(ItemType itemType) {
        
        Session session=HibernateActivity.getHibernateSession();
        
        if(session!=null)
        {   
            session.beginTransaction();
            session.save(itemType);
            session.getTransaction().commit();
            
            
            return true;
        }
        else
            return false;
        
    }
    
}
