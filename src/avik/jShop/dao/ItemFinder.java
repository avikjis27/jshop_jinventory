/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avik.jShop.dao;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import persistency.HibernateActivity;
import persistency.ItemMaster;


/**
 *
 * @author avik
 */
public class ItemFinder {
    
    
    
    
   public  static ArrayList getItems(String hints){
        
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        //Query query=session.createQuery("select distinct(a.itemModelNumber) from ItemMaster a where a.itemModelNumber like :ShortId");
        Query query=session.createSQLQuery("select distinct(a.item_model_v) from item_master a where upper(a.item_model_v) like :ShortId  and a.sold_v='N'");
        ArrayList list=(ArrayList) query.setParameter("ShortId","%"+hints.toUpperCase()+"%").list();
        System.out.println(list);
        return list;
        
        
    }
   public static boolean isItemExists(String itemModelNumber)
   {    Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        Query query=session.createQuery("select distinct(a.itemModelNumber) from ItemMaster a where upper(a.itemModelNumber)= :ShortId");
        ArrayList list=(ArrayList) query.setParameter("ShortId",itemModelNumber.toUpperCase()).list();
        System.out.println(list);
        if(list.size()==1)
        {
            return true;
        }
        else {
           return false;
       }
       
   }
   
    
    public static ArrayList getPossibleSerial(String modelNumber,String hints){
        
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        //Query query=session.createQuery("select a.itemId from ItemMaster a where a.itemModelNumber= :modelNumber and a.itemId like :ShortId");
        /*select * from item_details a where a.item_model_v like 'ysdiyaid' and a.item_serial_number_v like '28638216salas';*/
        Query query=session.createSQLQuery("select a.item_serial_number_v from item_master a where upper(a.item_model_v)= :modelNumber and upper(a.item_serial_number_v) like :ShortId and a.sold_v='N'");
        query.setString("modelNumber", modelNumber.toUpperCase());
        query.setString("ShortId", "%"+hints.toUpperCase()+"%");
        System.out.println(query.getQueryString());
        ArrayList list=(ArrayList) query.list();
        System.out.println(list);
        return list;
        
    }
    
    public static boolean isSerialExists(String modelNumber,String serialNumber)
    {   Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        Query query=session.createQuery("select a.itemId from ItemMaster a where upper(a.itemModelNumber)= :modelNumber and upper(a.itemId)= :ShortId");
        query.setString("modelNumber", modelNumber.toUpperCase());
        query.setString("ShortId", serialNumber.toUpperCase());
        System.out.println(query.getQueryString());
        ArrayList list=(ArrayList) query.list();
        System.out.println(list);
       if(list.size()==1)
        {
            return true;
        }
        else {
           return false;
       }
        
    }
    
    public static ItemMaster getItemDetails(String modelNumber,String serialNumber)
    {   
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        Query query=session.createQuery("from ItemMaster a where a.itemModelNumber= :modelNumber and a.itemId= :ShortId");
        query.setString("modelNumber", modelNumber);
        query.setString("ShortId", serialNumber);
        System.out.println(query.getQueryString());
        ArrayList list=(ArrayList) query.list();
        if(list.size()>1)
        {    System.out.println("Get confused as got two entry with one model number and serial number");
             return null;
        }
        else{
            System.out.println("Got one item with serial number:"+serialNumber+" ,and model number:"+modelNumber);
            return(ItemMaster) (list.get(0)) ;
        }
        
    }
    
    public static boolean purchaseItemProcess(String[][] details) throws SQLException
    {
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        Connection con=session.connection();
        CallableStatement cs=(CallableStatement) con.prepareCall("{call sold_item(?,?)}");
        
        
        for(int i=0;i<details.length;i++)
        {
            /*Query query=session.getNamedQuery("sold_item");
            query.setParameter(1,details[i][0] );
            query.setParameter(2,details[i][1] );*/
            cs.setString(1,details[i][0]);
            cs.setString(2,details[i][1]);
            cs.execute();
            //System.out.println("Update status for item:"+status);
        }
        
        session.getTransaction().commit();
        return true;
    }
    
    public static void generateInvoiceNumber(long id) throws SQLException
    {   System.out.println("Going to put invoice number");
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        Connection con=session.connection();
        CallableStatement cs=(CallableStatement) con.prepareCall("{call generate_invoice_details(?)}");
        cs.setLong(1,id);
        cs.execute();
        session.getTransaction().commit();
        
    }
    
    
    
    
    
}
