/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avik.jShop.exception;

/**
 *
 * @author avik
 */
public class DuplicateItemEntryException extends RuntimeException {
    
    private String message;
    private int errCode;
    
    public DuplicateItemEntryException(int errCode)
    {
        this.errCode=errCode;
    }

    @Override
    public String toString() {
       
        
        
        if(errCode==1000)
            return errCode+":Duplicate Item number is not allowed to add";
        else if(errCode==1001)
            return errCode+":No item in cart to remove";
        else
            return "Generic duplicate Entry Error";
    }
    
    
}
